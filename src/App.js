
import classes from'./App.module.css';
import MovieCard from './components/MovieCard';
import { Fragment, useEffect, useState } from 'react';


const App =()=>{
  const initialState = [{
    name: "Geetanjali", description:"Geetanjali movie description", cast:{hero:'Arya',heroine:'Kajol'}, similarMovies:['abc','xyz'], genre:'Love', language:"kannada" 
  },{
    name: "Geet", description:"geet movie descritpion", cast:{hero:'Arjun',heroine:'Samantha'}, similarMovies:['cbz','anb'], genre:'drama', language:"telugu" 
  },{
    name: "Geetanjali", description:"Sangu and Geet description ", cast:{hero:'Charan',heroine:'Anu'}, similarMovies:['abc','xyz'], genre:'thriller', language:"kannada" 
  }]
  let movieForm= {
    name: {
      elementType: 'input',
      value: '',
      elementConfig: {
        type: 'text',
        placeholder: 'Customer Name'
      },
    },
    description: {
      elementType: 'input',
      value: '',
      elementConfig: {
        type: 'text',
        placeholder: 'Customer Name'
      },
    },
    genre:{
      elementType: 'input',
      value: '',
      elementConfig: {
        type: 'text',
        placeholder: 'Customer Name'
      }
    },
    language:{
      elementType: 'input',
      value: '',
      elementConfig: {
        type: 'text',
        placeholder: 'Customer Name'
      },
    },
   }

  const languages =['All','Kannada','Telugu','Hindi','English']
  const [movies, setMovies]=useState([])
  const [Filteredmovies, setFilteredmovies]=useState([])
  const [SearchedInput, setSearchedInput]=useState([])
  const [Favorite , setFavorite ]=useState([])
  const [filterValue , setfilterValue ]=useState('All')
  const [Form ,setForm] =useState(movieForm)
  const [firstRender,setfirstRender]=useState(true)
  // useEffect(()=>{
  //   setMovies([...initialState])
  // },[])
  useEffect(()=>{
    // intialiazing state on first render
    if(firstRender){
      setfirstRender(false)
      setMovies([...initialState])
      return
    }
    //filtering the display movie list on filter and serach input changes
    filter()
  },[filterValue,SearchedInput,movies])

  function filter(){
    let result =[]
    if(SearchedInput!="" && filterValue!=='All'){
      result = movies.filter((movie)=> { return movie.language.toLocaleLowerCase() == filterValue.toLocaleLowerCase() &&( movie.name.toLocaleLowerCase() == SearchedInput.toLocaleLowerCase() || movie.genre.toLocaleLowerCase()== SearchedInput.toLocaleLowerCase())})
    } else if(SearchedInput!="" && filterValue=='All') {
      result = movies.filter((movie)=> { return movie.name.toLocaleLowerCase() == SearchedInput.toLocaleLowerCase() || movie.genre.toLocaleLowerCase()== SearchedInput.toLocaleLowerCase()})
    }else if(SearchedInput=="" && filterValue!=='All'){
      result = movies.filter((movie)=> { return movie.language.toLocaleLowerCase() == filterValue.toLocaleLowerCase()})
    }else {
      result =movies
    }
    setFilteredmovies([...result])
  }

  //searching onclick of search button 
  const searchMovie = (event)=>{
    event.preventDefault()
    filter() 
  }

  //Adding movie to favorite list and gives alert messsage if movie already exists in fav list
  const addToFavorite =( movie)=>{
    let exists = Favorite.find((fav)=>{ return fav.name=== movie.name})
    if(!exists){
    const newFavoriteList = [...Favorite , movie];
		setFavorite(newFavoriteList);
    }else{
      alert('Movie already Exists in favorites')
    }
  }
  //removing movie from favorite list
  const removeFromFavorite =(movie)=>{
    let updatedMovieList =Favorite.filter((fav)=>{return !fav.name=== movie.name})
    setFavorite([...updatedMovieList])

  }

  //Adding movie to list 
  const addMovie =(event)=>{
    event.preventDefault()
      let movie ={
        name: Form.name.value, description:Form.description.value,  genre:Form.genre.value, language:Form.language.value
      }
      console.log('inside adding movie',movie)
      setMovies([...movies, movie])
      setForm(movieForm)
  }
  //Handling form data
 const  inputChangehandler =(event, inputIdentifier)=>{
    let updateOrderForm={
        ...Form
    }
     let updateOrderElement={
        ...Form[inputIdentifier]
    }
    updateOrderElement.value=event.target.value;
    updateOrderForm[inputIdentifier]=updateOrderElement
    setForm({...updateOrderForm})
} 
// creating form for Adding movie
    let inputFields = []
    for(let key in Form) {
      inputFields.push({
        id: key,
        config: Form[key],
      })
    }
    let form = (
        <form onSubmit={addMovie}>
        {inputFields.map((element => {
          return (
            <div>
            <label  htmlFor={element.config.value} key={`${element.id}+'label'`}> {element.id} : </label>
              <input
                key={element.id}
                type={element.config.elementType}
                value={element.config.value}
                onChange={(event) => inputChangehandler(event,element.id)}
              />
              </div>
          )
        }))}
          <button>Add</button>
      </form>)
  return (
    <Fragment>
      <div className={classes.Appheader}>
                    <form className="search-form" onSubmit={searchMovie}>
                        <label
                            htmlFor="SearchedInput"
                        >
                        </label>
                        <input
                            type="text"
                            name="SearchedInput"
                            placeholder="search"
                            value={SearchedInput}
                            onChange={(e) => setSearchedInput(e.target.value)}
                        />
                        <button type="submit">Search</button>
                    </form>
              
    <select
      onChange={(e) => {
        setfilterValue(e.target.value);
       }}>
        {languages.map((language, index) => (
        <option value={language} key={index}>{language}</option>
        ))
        }
        </select>
        </div>
        <div className={classes.AddMovie}>
        <h4> Fill details to add movie </h4>
        { form}
        </div>
      <h2> Movies </h2>
      <MovieCard movies={Filteredmovies} addToFavoriteFlag ="true" addToFavorite={addToFavorite}/>
      <br>
      </br>
      <h2> Favorite Movies </h2>
      <MovieCard movies={Favorite}   removeFromFavorite={removeFromFavorite}/>
    </Fragment>
  )
}

export default App;
