
import classes from './MovieCard.module.css'

//Movie card for displaying the movie details
const MovieCard = (props) => {
	return (
		<div className={classes.cardContainer}>
			{props.movies.map((movie, index) => (
				<div key={index} className={classes.movieCard}>
	                { props.addToFavoriteFlag ? <button onClick={()=>props.addToFavorite(movie)}> Add to Favorite</button> :<button onClick={()=>props.removeFromFavorite(movie)}> Remove from Favorite</button>}
                    <p>Name: {movie.name}</p>
                    <p>Description: {movie.description}</p>
					<p>Cast: {movie.cast.hero}, {movie.cast.heroine}</p>
                    <p>Genre: {movie.genre}</p>
                    <p>Language: {movie.language}</p>
				</div>
			))}
		</div>
	);
};

export default MovieCard;